#!/usr/bin/env bash

MSG_RELAY_URL="http://10.203.55.74:30050/api/v1"

# Use host network for testing only
docker run --net=host --rm -e MSG_RELAY_URL=$MSG_RELAY_URL github-listener 
